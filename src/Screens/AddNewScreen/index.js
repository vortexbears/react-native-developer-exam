import * as React from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    FlatList,
    Image,
    TouchableWithoutFeedback,
    Button,
} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-picker';
import axios from 'axios';

class AddNewScreen extends React.Component {
    static navigationOptions = {
        title: 'Add Screen',
    };

    state = {
        photo: null,
        title: null
    }

    handleChoosePhoto = () => {
        const options = {
            noData: true
        }

        ImagePicker.launchImageLibrary(options, response => {

            if (response.uri) {
                this.setState({
                    photo: response
                });
            }
        });
    }

    handleSubmit = async () => {
        try {

            if(this.state.photo) {
            const response = await axios.post('https://jsonplaceholder.typicode.com/photos', {
                body: JSON.stringify({
                    title: this.state.title,
                    url: this.state.photo.uri,
                    id: Date.now()
                }),
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                }
            });

            alert('Success');

            this.setState({
                title: null,
                photo: null
            })
            } else {
                alert('please attach a photo');
            }
           
        } catch (error) {
            console.log(error);
        }
    }   

    render() {

        const { photo } = this.state;

        return (
        <View style={styles.container}>
            <View style={{ borderRadius: 12, marginHorizontal: 20, marginVertical: 20, width: '90%', backgroundColor: '#ffffff', flex: 1, alignItems: "center", alignContent: 'center', justifyContent: 'center'}}>
                <View>
                    <Text style={{fontSize: 22}}>Add New Item</Text>
                </View>
                <TextInput
                    style={{borderRadius: 8,paddingLeft: 10, fontSize: 16, marginTop: 15,width: 300, height: 40, borderColor: 'gray', borderWidth: 1, backgroundColor: '#ffffff' }}
                    onChangeText={(title) => this.setState({title})}
                    placeholder="Input Title"
                    value={this.state.title}
                />
                {
                    (photo) ? <Image 
                        style={{width: 100, height: 100, marginTop: 15}}
                        source={{uri: photo.uri}}
                    /> : null
                }

                <View style={{marginTop: 15}}>
                    <Button title="Choose Photo" onPress={this.handleChoosePhoto} />
                </View>
                <View style={{marginTop: 15}}>
                    <Button title="Submit" color="#2416EB" onPress={this.handleSubmit}/>
                </View>
            </View>
        </View>
        );
    }

    _showMoreApp = () => {
        this.props.navigation.navigate('Other');
    };
 
}
  
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#cccccc'
    },
});

export default AddNewScreen;