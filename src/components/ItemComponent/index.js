import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  FlatList,
  Image,
  TouchableWithoutFeedback,
  Button,
} from 'react-native';



class Item extends Component {

    render() {

        const { item } = this.props;

        return(
            <TouchableWithoutFeedback onPress={() => this.props._onClickDetailScreen(item)}>
            <View style={styles.container}>
                <View style={{flex: 1, flexDirection: "row", alignItems: 'center'}}>
                    <View style={{flex: 1, flexBasis: "90%"}}>
                        <Text style={{fontSize: 16}}>{item.title}</Text>
                    </View>
                    <View style={{flex: 1, flexBasis: "10%", alignItems: 'flex-end'}}>
                    <TouchableWithoutFeedback onPress={() => this.props.deleteItem(item.id)}>
                        <Text style={{textAlign: "right", fontSize: 22, color: '#EB2C20'}}>x</Text>
                    </TouchableWithoutFeedback>
                    </View>
                </View>
                <View style={{marginTop: 10, flex: 1, flexDirection: 'row', alignItems: "center"}}>
                    <View style={{flex: 1, flexBasis: '30%'}}>
                    <Image
                        style={{width: 100, height: 100}}
                        source={{uri: item.url}}
                    />
                    </View>
                    <View style={{flex: 1, flexBasis: '70%'}}>
                        <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Text>
                    </View>
                </View>
            </View>
            </TouchableWithoutFeedback>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 10,
        marginRight: 10,
        padding: 15,
        borderRadius: 12,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 2 },
        shadowOpacity: 0.4,
        shadowRadius: 2,
        elevation: 1,
    }
})

export default Item;