# React Native Developer Exam

Step 1:
Create a Gitlab account and have us add you to the exam repository: https://gitlab.com/sven-exams/react-native-developer-exam (If you're having trouble, don't hesitate to ask for assistance)

Step 2:
Clone react-native-developer-exam repository and create your own branch with the following
format: firstnamelastname, ie: richmondko

Step 3:
Using any public API, create a mobile app (using React Native platform) that displays a gallery or listing of any topic or thing you like (e.g movies, games, music, books, etc.). The app should fulfill the following requirements:

- As a user, I should be able to see the list of entries on the first page
- Each entry must have a title, image or icon, and a short description
- The list should be paginated, not all entries should be loaded initially 
- As a user, I should be able to see a detail view of an entry
- Detail view should have an image and the entry's corresponding data
- As a user, I should be able to add a new entry to the list
- As a user, I should be able to edit the details of an entry
- As a user, I should be able to delete an entry from the list

Step 4:
Once your app is done, push your branch and create a merge request and mention
@mlua, title of the merge request is Firstname Lastname React Native Dev Exam

<!---->

For stable version use node v12.10.0.